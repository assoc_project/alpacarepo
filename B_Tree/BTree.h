#ifndef _BTREE_H_
#define _BTREE_H_

#include <iostream>
#include <vector>
#include <memory>
#include <cassert>
#define T 3

template <typename KeyType, typename DataType>
class BTree
{
private:
    struct Node
    {
        KeyType key;
        DataType data;
        bool isLeaf;
        std::vector<std::shared_ptr<Node>> children;
        std::shared_ptr<Node> parent;
        Node(): isLeaf(false), parent(nullptr){}
    };
    std::shared_ptr<Node> root;
    size_t treeSize;

    BTree() = delete;


    void splitIfRoot(const std::shared_ptr<Node> curRoot);


    void split(const std::shared_ptr<Node> curRoot);


    std::shared_ptr<Node> findSpaceInChildren(const KeyType& newKey, const std::shared_ptr<Node> curRoot) const;


    std::shared_ptr<Node> findInChildren(const KeyType& newKey, const std::shared_ptr<Node> curRoot) const;


    void merge(const std::shared_ptr<Node> leftHalf, const std::shared_ptr<Node> rightHalf);


    void stealFromLeftNeighbour(const std::shared_ptr<Node> curRoot, const std::shared_ptr<Node> leftNeighbour);


    void stealFromRightNeighbour(const std::shared_ptr<Node> curRoot, const std::shared_ptr<Node> rightNeighbour);


    std::shared_ptr<Node> findFromRoot(const KeyType& newKey, const std::shared_ptr<Node> curRoot) const;


    std::shared_ptr<Node> findNodeToErase(const KeyType& delKey, const std::shared_ptr<Node> startRoot);


    void eraseFromLeaf(const KeyType& delKey, const std::shared_ptr<Node> startRoot);


    void eraseFromSetRoot(const KeyType& delKey, const std::shared_ptr<Node> startRoot);


public:
    class BTreeIterator
    {
    public:


        BTreeIterator():iter(nullptr), root(nullptr){}


        BTreeIterator(const typename std::vector<std::shared_ptr<Node>>::iterator& newIt, const std::shared_ptr<Node>& newRoot): iter(newIt), root(newRoot){}


        BTreeIterator(std::nullptr_t null): iter(null), root(null){}


        BTreeIterator& operator--();



        BTreeIterator& operator++();



        bool operator!= (BTreeIterator anotherIter) const
        {
            return (iter != anotherIter.iter || root != anotherIter.root);
        }
        bool operator== (BTreeIterator anotherIter) const
        {
            return (iter == anotherIter.iter && root == anotherIter.root);
        }


        typename std::vector<std::shared_ptr<Node>>::iterator operator->()
        {
            return iter;
        }



        std::shared_ptr<Node>& operator* ()
        {
            return *iter;
        }


    private:
        std::shared_ptr<Node> root;
        typename std::vector<std::shared_ptr<Node>>::iterator iter;
    };
    typedef BTreeIterator iterator;


    iterator begin() const
    {
        auto curRoot = root -> children.begin();
        while((*curRoot) -> children.size() >= 2 * T - 1)
        {
            curRoot = (*curRoot) -> children.begin();
        }
        iterator iter(curRoot + 1, root);
        return iter;
    }


    iterator end() const
    {
        iterator iter(root -> children.end(), root);
        return iter;;
    }


private:
    iterator findIterFromRoot(const KeyType& newKey, const std::shared_ptr<Node> curRoot) const;


public:

    void insert(const KeyType& newKey, const DataType& newData);


    iterator find(const KeyType& newKey) const
    {
        return findIterFromRoot(newKey, root);
    }


    void erase(const KeyType& delKey)
    {
        eraseFromSetRoot(delKey, root);
    }


    size_t size()
    {
        return treeSize;
    }


    DataType& operator [](const KeyType& keyToFind) const
    {
        auto resNode = find(keyToFind);
        return (*resNode) -> data;
    }


    BTree(const KeyType& newKey, const DataType& newData)
    {
        root = std::make_shared<Node>();
        root -> children.resize(3);
        root -> children[0] = std::make_shared<Node>();
        root -> children[0] -> isLeaf = false;
        root -> children[1] = std::make_shared<Node>();
        root -> children[1] -> isLeaf = true;
        root -> children[2] = std::make_shared<Node>();
        root -> children[2] -> isLeaf = false;
        root -> children[0] -> parent = root;
        root -> children[1] -> parent = root;
        root -> children[2] -> parent = root;
        root -> children[1] -> key = newKey;
        root -> children[1] -> data = newData;
        treeSize = 1;
    }
};


template <typename KeyType, typename DataType>
void BTree<KeyType, DataType>::splitIfRoot(const std::shared_ptr<Node> curRoot)
{
    auto newPtr = std::make_shared<Node>();
    newPtr -> children.resize(3);
    newPtr -> children[0] = std::make_shared<Node>();
    newPtr -> children[2] = std::make_shared<Node>();
    root -> children[0] -> isLeaf = false;
    root -> children[2] -> isLeaf = false;
    auto leftHalf = newPtr -> children[0];
    auto rightHalf = newPtr -> children[2];
    leftHalf -> parent = newPtr;
    leftHalf -> children.resize(2 * T - 1);
    for(size_t index = 0; index < 2 * T - 1; ++index)
    {
        leftHalf -> children[index] = curRoot -> children[index];
        leftHalf -> children[index] -> parent = leftHalf;
    }
    rightHalf -> children.resize(2 * T - 1);
    rightHalf -> parent = newPtr;
    for(size_t index = 0; index < 2 * T - 1; ++index)
    {
        rightHalf -> children[index] = curRoot -> children[2 * T + index];
        rightHalf -> children[index] -> parent = rightHalf;
    }
    newPtr -> children[1] = curRoot;
    curRoot -> key = curRoot -> children[2 * T - 1] -> key;
    curRoot -> data = curRoot -> children[2 * T - 1] -> data;
    curRoot -> parent = newPtr;
    root = newPtr;
    curRoot -> children.clear();
    curRoot -> isLeaf = true;
}

template <typename KeyType, typename DataType>
void BTree<KeyType, DataType>::split(const std::shared_ptr<Node> curRoot)
{
    if(curRoot -> parent == nullptr)
    {
        splitIfRoot(curRoot);
    }
    else
    {
        auto parent = curRoot -> parent;
        auto leftHalf = std::make_shared<Node>();
        leftHalf -> isLeaf = false;
        leftHalf -> parent = parent;
        auto rightHalf = std::make_shared<Node>();
        rightHalf -> isLeaf = false;
        rightHalf -> parent = parent;
        auto it = parent -> children.begin();
        for(it = parent -> children.begin(); it != parent -> children.end() && (*it) != curRoot; it++);
        parent -> children.insert(it, leftHalf);
        for(it = parent -> children.begin(); it != parent -> children.end() && (*it) != curRoot; it++);
        parent -> children.insert(it + 1, rightHalf);
        leftHalf -> children.resize(2 * T - 1);
        for(size_t index = 0; index < 2 * T - 1; ++index)
        {
            leftHalf -> children[index] = curRoot -> children[index];
            leftHalf -> children[index] -> parent = leftHalf;
        }
        rightHalf -> children.resize(2 * T - 1);
        for(size_t index = 0; index < 2 * T - 1; ++index)
        {
            rightHalf -> children[index] = curRoot -> children[2 * T + index];
            rightHalf -> children[index] -> parent = rightHalf;
        }
        curRoot -> key = curRoot -> children[2 * T - 1] -> key;
        curRoot -> data = curRoot -> children[2 * T - 1] -> data;
        curRoot -> children.clear();
        curRoot -> isLeaf = true;
    }
}


template <typename KeyType, typename DataType>
std::shared_ptr<typename BTree<KeyType, DataType>::Node> BTree<KeyType, DataType>::findSpaceInChildren(const KeyType& newKey, const std::shared_ptr<Node> curRoot) const
{
    if(newKey < curRoot -> children[1] -> key)
    {
        return curRoot -> children[0];
    }
    if(newKey > curRoot -> children[curRoot -> children.size() - 2] -> key)
    {
        return curRoot -> children[curRoot -> children.size() - 1];
    }
    assert(newKey != curRoot -> children[curRoot -> children.size() - 2] -> key);
    for(size_t index = 1; index < curRoot -> children.size() - 2; index += 2)
    {
        assert(newKey != curRoot -> children[index] -> key);
        if(newKey < curRoot -> children[index + 2] -> key && newKey > curRoot -> children[index] -> key)
        {
            return curRoot -> children[index + 1];
        }
    }
}

template <typename KeyType, typename DataType>
std::shared_ptr<typename BTree<KeyType, DataType>::Node> BTree<KeyType, DataType>::findInChildren(const KeyType& newKey, const std::shared_ptr<Node> curRoot) const
{
    if(newKey < curRoot -> children[1] -> key)
    {
        return curRoot -> children[0];
    }
    if(newKey > curRoot -> children[curRoot -> children.size() - 2] -> key)
    {
        return curRoot -> children[curRoot -> children.size() - 1];
    }
    if(newKey == curRoot -> children[curRoot -> children.size() - 2] -> key)
    {
        return curRoot -> children[curRoot -> children.size() - 2];
    }
    for(size_t index = 1; index < curRoot -> children.size() - 2; index += 2)
    {
        if(newKey == curRoot -> children[index] -> key)
        {
            return curRoot -> children[index];
        }
        if(newKey < curRoot -> children[index + 2] -> key && newKey > curRoot -> children[index] -> key)
        {
            return curRoot -> children[index + 1];
        }
    }
}


template <typename KeyType, typename DataType>
void BTree<KeyType, DataType>::merge(const std::shared_ptr<Node> leftHalf, const std::shared_ptr<Node> rightHalf)
{
    auto parent = rightHalf -> parent;
    auto midNode = parent;
    for(size_t index = 0; parent -> children[index] != rightHalf; index += 2)
    {
        if(parent -> children[index] == leftHalf)
        {
            midNode = parent -> children[index + 1];
        }
    }
    midNode -> children.resize(T * 4 - 1);
    for(size_t index = 0; index < 2 * T - 1; ++index)
    {
        midNode -> children[index] = leftHalf -> children[index];
        midNode -> children[index] -> parent = midNode;
    }
    for(size_t index = 0; index < 2 * T - 1; ++index)
    {
        midNode -> children[2 * T + index] = rightHalf -> children[index];
        midNode -> children[2 * T + index] -> parent = midNode;
    }
    midNode -> children[2 * T - 1] = std::make_shared<Node>();
    midNode -> children[2 * T - 1] -> key = midNode -> key;
    midNode -> children[2 * T - 1] -> data = midNode -> data;
    midNode -> children[2 * T - 1] -> isLeaf = true;
    midNode -> children[2 * T - 1] -> parent = midNode;
    midNode -> isLeaf = false;
    for(auto it = parent -> children.begin(); it != parent -> children.end(); ++it)
    {
        if((*it) == leftHalf)
        {
            parent -> children.erase(it);
            it = parent -> children.begin() - 1;
        }
        else
        {
            if((*it) == rightHalf)
            {
                parent -> children.erase(it);
                it = parent -> children.end() - 1;
            }
        }
    }
    if(root -> children.size() == 1)
    {
        root = root -> children[0];
        root -> parent = nullptr;
    }
}

template <typename KeyType, typename DataType>
void BTree<KeyType, DataType>::stealFromLeftNeighbour(const std::shared_ptr<Node> curRoot, const std::shared_ptr<Node> leftNeighbour)
{
    auto parent = curRoot -> parent;
    auto midNode = parent;
    size_t index;
    for(index = 0; parent -> children[index] != curRoot; index += 2)
    {
        if(parent -> children[index] == leftNeighbour)
        {
            midNode = parent -> children[index + 1];
            break;
        }
    }
    curRoot -> children.insert(curRoot -> children.begin(), midNode);
    midNode -> parent = curRoot;
    curRoot -> children.insert(curRoot -> children.begin(), *(leftNeighbour -> children.end() - 1));
    (*(leftNeighbour -> children.end() - 1)) -> parent = curRoot;
    leftNeighbour -> children.pop_back();
    auto newMidNode = *(leftNeighbour -> children.end() - 1);
    newMidNode -> parent = parent;
    parent -> children[index + 1] = newMidNode;
    leftNeighbour -> children.pop_back();
}


template <typename KeyType, typename DataType>
void BTree<KeyType, DataType>::stealFromRightNeighbour(const std::shared_ptr<Node> curRoot, const std::shared_ptr<Node> rightNeighbour)
{
    auto parent = curRoot -> parent;
    auto midNode = parent;
    size_t index;
    for(index = 0; parent -> children[index] != rightNeighbour; index += 2)
    {
        if(parent -> children[index] == curRoot)
        {
            midNode = parent -> children[index + 1];
            break;
        }
    }
    curRoot -> children.push_back(midNode);
    midNode -> parent = curRoot;
    curRoot -> children.push_back(*(rightNeighbour -> children.begin()));
    (*(rightNeighbour -> children.begin())) -> parent = curRoot;
    rightNeighbour -> children.erase(rightNeighbour -> children.begin());
    auto newMidNode = *(rightNeighbour -> children.begin());
    newMidNode -> parent = parent;
    parent -> children[index + 1] = newMidNode;
    rightNeighbour -> children.erase(rightNeighbour -> children.begin());
}


template <typename KeyType, typename DataType>
std::shared_ptr<typename BTree<KeyType, DataType>::Node> BTree<KeyType, DataType>::findFromRoot(const KeyType& newKey, const std::shared_ptr<Node> curRoot) const
{
    if(curRoot -> children.size() < 2)
    {
        if(curRoot -> key == newKey && curRoot -> isLeaf)
        {
            return curRoot;
        }
        else
        {
            return nullptr;
        }
    }
    else
    {
        return findFromRoot(newKey, findInChildren(newKey, curRoot));
    }
}


template <typename KeyType, typename DataType>
std::shared_ptr<typename BTree<KeyType, DataType>::Node> BTree<KeyType, DataType>::findNodeToErase(const KeyType& delKey, const std::shared_ptr<Node> startRoot)
{
    auto curRoot = startRoot;
    auto parent = curRoot -> parent;
    size_t index = 0;
    bool changed = false;
    while(curRoot -> children.size() >= (2 * T - 1) || curRoot == root)
    {
        if(curRoot -> children.size() == 2 * T - 1 && curRoot != root)
        {
            while(parent -> children[index] != curRoot)
            {
                ++index;
            }
            if(index > 0 && parent -> children[index - 2] -> children.size() != 2 * T - 1)
            {
                stealFromLeftNeighbour(curRoot, parent -> children[index - 2]);
                changed = true;
            }
            if(!changed && index < 2 * T - 2 && index + 2 < parent -> children.size() && parent -> children[index + 2] -> children.size() != 2 * T - 1)
            {
                stealFromRightNeighbour(curRoot, parent -> children[index + 2]);
                changed = true;
            }
            if(index > 0 && !changed)
            {
                merge(parent -> children[index - 2], curRoot);
                if(parent -> children[0] == root)
                {
                    curRoot = root;
                }
                else
                {
                    curRoot = parent;
                }
                changed = true;
            }
            if(index < 2 * T - 2 && index + 2 < parent -> children.size() && !changed)
            {
                merge(curRoot, parent -> children[index + 2]);
                if(parent -> children[0] == root)
                {
                    curRoot = root;
                }
                else
                {
                    curRoot = parent;
                }
            }
            parent = curRoot -> parent;
            changed = false;
            index = 0;
        }
        else
        {
            curRoot = findInChildren(delKey, curRoot);
            parent = curRoot -> parent;
        }
    }
    return curRoot;
}


template <typename KeyType, typename DataType>
void BTree<KeyType, DataType>::eraseFromLeaf(const KeyType& delKey, const std::shared_ptr<Node> startRoot)
{
    size_t index = 0;
    auto curRoot = findNodeToErase(delKey, startRoot);
    auto parent = curRoot -> parent;
    if(curRoot -> children.size() == 0 && parent -> children[0] -> children.size() == 0)
    {
        while(parent -> children[index] -> key != delKey)
        {
            ++index;
        }
        parent -> children.erase(parent -> children.begin() + index);
        parent -> children.erase(parent -> children.begin() + index);
        treeSize--;
        return;
    }
}

template <typename KeyType, typename DataType>
void BTree<KeyType, DataType>::eraseFromSetRoot(const KeyType& delKey, const std::shared_ptr<Node> startRoot)
{
    size_t index = 0;
    bool changed = false;
    auto curRoot = findNodeToErase(delKey, startRoot);
    assert(curRoot -> isLeaf);
    auto parent = curRoot -> parent;
    auto delNode = curRoot;
    while(parent -> children[index] != curRoot)
    {
        ++index;
    }
    while(parent -> children[index - 1] -> children.size() == 2 * T - 1 && parent -> children[index + 1] -> children.size() == 2 * T - 1)
    {
        merge(parent -> children[index - 1], parent -> children[index + 1]);
        parent = parent -> children[index - 1];
    }
    if(changed == false && parent -> children[index - 1] -> children.size() > 2 * T - 1)
    {
        delNode = parent -> children[index - 1];
        while(delNode -> children.size() != 0)
        {
            delNode = delNode -> children[delNode -> children.size() - 1];
        }
        delNode = delNode -> parent;
        delNode = delNode -> children[delNode -> children.size() - 2];
        changed = true;
        eraseFromLeaf(delNode -> key, parent);
    }
    if(changed == false && parent -> children[index + 1] -> children.size() > 2 * T - 1)
    {
        delNode = parent -> children[index + 1];
        while(delNode -> children.size() != 0)
        {
            delNode = delNode -> children[0];
        }
        delNode = delNode -> parent;
        delNode = delNode -> children[1];
        eraseFromLeaf(delNode -> key, parent);
        changed = true;
    }
    if(changed == false)
    {
        eraseFromLeaf(curRoot -> key, curRoot);
    }
    curRoot -> key = delNode -> key;
    curRoot -> data = delNode -> data;
    return;
}

template <typename KeyType, typename DataType>
typename BTree<KeyType, DataType>::iterator& BTree<KeyType, DataType>::iterator::operator--()
{
    auto it = iter;
    if(iter == root -> children.end())
    {
        ----iter;
        return *this;
    }
    auto parent = (*iter) -> parent;
    auto curRoot = *iter;
    int index = 0;
    while(parent -> children[index] != curRoot)
    {
        ++index;
    }
    if(index > 1 && (*(iter - 1)) -> children.size() == 0)
    {
        iter = parent -> children.begin() + (index - 2);
    }
    else
    {
        curRoot = *(iter - 1);
        if (curRoot -> children.size() == 0)
        {
            while(true)
            {
                parent = curRoot -> parent;
                for(it = parent -> children.begin(); *it != curRoot; ++it);
                if(parent -> parent == nullptr)
                {
                    iter = it - 1;
                    return *this;
                }
                if(it > parent -> children.begin() + 1)
                {
                    iter = it - 1;
                    return *this;
                }
                else
                {
                    curRoot = parent;
                }
            }
        }
        else
        {
            while(!curRoot -> isLeaf)
            {
                it = curRoot -> children.end();
                if((*it) -> children.size() == 0)
                {
                    iter = it - 1;
                    return *this;
                }
                curRoot = (*it);
            }
        }
    }
}


template <typename KeyType, typename DataType>
typename BTree<KeyType, DataType>::iterator& BTree<KeyType, DataType>::iterator::operator++()
{
    auto it = iter;
    auto parent = (*iter) -> parent;
    auto curRoot = (*iter);
    size_t index = 0;
    if(iter == (root -> children.begin() - 1))
    {
        ++++iter;
        return *this;
    }
    while(parent -> children[index] != curRoot)
    {
        ++index;
    }
    if(index < parent -> children.size() - 2 && (*(iter + 1)) -> children.size() == 0)
    {
        iter = parent -> children.begin() + index + 2;
        return *this;
    }
    else
    {
        curRoot = *(iter + 1);
        if (curRoot -> children.size() == 0)
        {
            while(true)
            {
                parent = curRoot -> parent;
                for(it = parent -> children.begin(); *it != curRoot; ++it);
                if(parent -> parent == nullptr)
                {
                    iter = it + 1;
                    return *this;
                }
                if(it + 1 < parent -> children.end())
                {
                    iter = it + 1;
                    return *this;
                }
                else
                {
                    curRoot = parent;
                }
            }
        }
        else
        {
            while(!curRoot -> isLeaf)
            {
                it = curRoot -> children.begin();
                if((*it) -> children.size() == 0)
                {
                    iter = it + 1;
                    return *this;
                }
                curRoot = (*it);
            }
        }
    }
}


template <typename KeyType, typename DataType>
typename BTree<KeyType, DataType>::iterator BTree<KeyType, DataType>::findIterFromRoot(const KeyType& newKey, const std::shared_ptr<Node> curRoot) const
{
    if(curRoot -> children.size() == 0)
    {
        if(curRoot -> key == newKey && curRoot -> isLeaf)
        {
            auto iter = begin();
            while((*iter) -> key != newKey)
            {
                ++iter;
            }
            return iter;
        }
        else
        {
            return nullptr;
        }
    }
    else
    {
        return findIterFromRoot(newKey, findInChildren(newKey, curRoot));
    }
}

template <typename KeyType, typename DataType>
void BTree<KeyType, DataType>::insert(const KeyType& newKey, const DataType& newData)
{
    auto curRoot = root;
    auto parent = curRoot -> parent;
    while(curRoot -> children.size() != 0)
    {
        if(curRoot == root)
        {
            if(curRoot -> children.size() == 4 * T - 1)
            {
                split(root);
                curRoot = root;
            }
            else
            {
                curRoot = findSpaceInChildren(newKey, curRoot);
            }
        }
        else
        {
            if(curRoot -> children.size() == 4 * T - 1)
            {
                parent = curRoot -> parent;
                split(curRoot);
                curRoot = parent;
            }
            else
            {
                curRoot = findSpaceInChildren(newKey, curRoot);
            }
        }
    }
    parent = curRoot -> parent;
    auto leftHalf = std::make_shared<Node>();
    leftHalf -> isLeaf = false;
    auto rightHalf = std::make_shared<Node>();
    rightHalf -> isLeaf = false;
    leftHalf -> parent = parent;
    rightHalf -> parent = parent;
    auto it = (parent -> children).begin();
    for(it = parent -> children.begin(); it != parent -> children.end() && (*it) != curRoot; it++);
    parent -> children.insert(it, leftHalf);
    for(it = parent -> children.begin(); it != parent -> children.end() && (*it) != curRoot; it++);
    parent -> children.insert(it + 1, rightHalf);
    curRoot -> key = newKey;
    curRoot -> data = newData;
    curRoot -> isLeaf = true;
    treeSize++;
}



#endif // _BTREE_H_
