#include "BTree.h"
#include <string>

int main()
{
    BTree<std::string, int> btree("A", 6);
    btree.insert("C", 6);
    btree.insert("B", 4);
    btree.insert("X", 5);
    std::cout << btree["A"] << "\n";
    std::cout << btree["X"] << "\n";
    for(auto it = (--btree.end()); it != (--btree.begin()); --it)
    {
        std::cout << (*it) -> data << "\n";
    }
    auto iter = btree.find("B");
    std::cout << (*iter) -> data;
    return 0;
}
