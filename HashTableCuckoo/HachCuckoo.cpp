#include <functional>
#include <iostream>
#include <vector>
#include <ctime>

using namespace std;

const int firstTableSize = 16;
const int hashParameter = 30011;

template<class KeyType, class DataType>
class HashTable
{
private:
    vector<pair<KeyType, DataType> *> table;

	long long keys_count;
    size_t table_size;
   
	long long second_a;
    long long second_b;
    long long first_a;
    long long first_b;
    
    long long getSecondHash(const KeyType&) const;
    long long getFirstHash(const KeyType&) const;
    void getNewParameters();
    
    unsigned int hashing(const KeyType&) const;
    
    void rehash();
public:
    class Iterator;

    HashTable()
    {
        table_size = firstTableSize;
        keys_count = 0;

        table = vector<pair<KeyType, DataType>*>(table_size + 1);

        for(int i = 0; i < table_size + 1; i++)
            table[i] = nullptr;

        getNewParameters();
    }

    ~HashTable()
    {
        for (int i = 0; i < table_size + 1; i++)
            delete table[i];
    }

    void insert(const KeyType& key, const DataType& value);
    Iterator find(const KeyType& key) const;
    void replace(const KeyType& key);

    class Iterator
    {
    private:
    	const HashTable<KeyType, DataType>* myTable;
        int idx;   
    public:
        int getIndex()
        {
            return idx;
        }

        Iterator(const HashTable<KeyType, DataType>* _myTable, int _idx)
        {
            myTable = _myTable;
            idx = _idx;
        }

        ~Iterator() {}

        pair<KeyType, DataType> operator *() const
        {
            pair<KeyType, DataType> temp(myTable->table[idx]->first, myTable->table[idx]->second);
            return temp;
        }
        
        Iterator& operator ++ ()
        {
            ++idx;
            if (Iterator(myTable, idx) != myTable->end())
                while ((myTable->table[idx] == nullptr) && (idx != myTable->table_size))
                    ++idx;
            return *this;
        }
        
        Iterator& operator -- ()
        {
            --idx;
            while(myTable->table[idx] == nullptr)
                --idx;
            return *this;
        }
        
        bool operator != (const Iterator& other) const
        {
            return (idx != other.idx);
        }
        
        bool operator ==(const Iterator& other) const
        {
            return (idx == other.idx);
        }
    };

    Iterator begin() const;
    Iterator end() const;
};

template<class KeyType, class DataType>
void HashTable<KeyType, DataType>::
	getNewParameters()
{
    srand(time(nullptr));
    first_a = rand() % (hashParameter - 1) + 1;
    first_b = rand() % hashParameter;
    second_a = rand() % (hashParameter - 1) + 1;
    second_b = rand() % hashParameter;

    while((first_a == first_b) && (second_a == second_b))
    {
        first_a = rand() % (hashParameter - 1) + 1;
        first_b = rand() % hashParameter;
        second_a = rand() % (hashParameter - 1) + 1;
        second_b = rand() % hashParameter;
    }
}

template<class KeyType, class DataType>
unsigned int HashTable<KeyType, DataType>::
	hashing(const KeyType& key) const
{
    hash<KeyType> hashFunction;
    unsigned int result = hashFunction(key);
    return result;
}

template<class KeyType, class DataType>
long long HashTable<KeyType, DataType>::
	getFirstHash(const KeyType& key) const
{
    unsigned long long hash_value = 0;
    unsigned int hash_key = hashing(key);
    hash_value = hash_key % hashParameter;
    hash_value = ((first_a * hash_value + first_b) % hashParameter) % (table_size / 2);
    return hash_value;
}

template<class KeyType, class DataType>
long long HashTable<KeyType, DataType>::
	getSecondHash(const KeyType& key) const
{
    unsigned long long hash_value = 0;
    unsigned int hash_key = hashing(key);
    hash_value = hash_key % hashParameter;
    hash_value = ((second_a * hash_value + second_b) % hashParameter) % (table_size / 2) + (table_size / 2);
    return hash_value;
}

template<class KeyType, class DataType>
void HashTable<KeyType, DataType>::
	insert(const KeyType &key, const DataType &value)
{
    if(find(key) != this->end())
    {
        return;
    }

    if(4 * keys_count > 3 * table_size)
    {
        rehash();
    }

    pair<KeyType, DataType>* temp = new pair<KeyType, DataType>(key, value);
    int count_steps = 0;

    while(4 * (++count_steps) < 3 * (table_size  / 2))
    {
        long long first_hash = getFirstHash(temp->first);
        if(table[first_hash] == nullptr)
        {
            keys_count++;
            table[first_hash] = temp;
            return;
        }
        swap(table[first_hash], temp);
        long long second_hash = getSecondHash(temp->first);
        if(table[second_hash] == nullptr)
        {
            keys_count++;
            table[second_hash] = temp;
            return;
        }
        swap(table[second_hash], temp);
    }

	rehash();
    // cout << "done" << endl;
    insert(temp->first, temp->second);
}

template<class KeyType, class DataType>
typename HashTable<KeyType, DataType>::
	Iterator HashTable<KeyType, DataType>::find(const KeyType &key) const
{
    long long first_hash = getFirstHash(key);
    long long second_hash = getSecondHash(key);
    if((table[first_hash] != nullptr) && (table[first_hash]->first == key))
    {
        return Iterator(this, first_hash);
    }
    else if ((table[second_hash] != nullptr) && (table[second_hash]->first == key))
    {
        return Iterator(this, second_hash);
    }
    return this->end();
}

template<class KeyType, class DataType>
void HashTable<KeyType, DataType>::
	replace(const KeyType &key)
{
    long long first_hash = getFirstHash(key);
    long long second_hash = getSecondHash(key);
    if((table[first_hash] != nullptr) && (table[first_hash]->first == key))
    {
        delete table[first_hash];
        table[first_hash] = nullptr;
        keys_count--;
    }
    else if((table[second_hash] != nullptr) && (table[second_hash]->first == key))
    {
        delete table[second_hash];
        table[second_hash] = nullptr;
        keys_count--;
    }
    return;
}

template<class KeyType, class DataType>
void HashTable<KeyType, DataType>::
	rehash()
{
    vector<pair<KeyType, DataType>*> old_table = vector<pair<KeyType, DataType>*>(2 * table_size + 1);

    for (int i = 0; i < 2 * table_size + 1; i++)
    {
        old_table[i] = nullptr;
    }

    swap(old_table, table);
    keys_count = 0;
    table_size *= 2;

    getNewParameters();

    for (int i = 0; i < table_size / 2; i++)
    {
        if (old_table[i] != nullptr)
        {
            insert(old_table[i]->first, old_table[i]->second);
        }
        delete old_table[i];
    }
}

template<class KeyType, class DataType>
typename HashTable<KeyType, DataType>::
	Iterator HashTable<KeyType, DataType>::begin() const
{
    int i = 0;
    while((table[i] == nullptr) && (i != table_size))
    {
        i++;
    }
    return Iterator(this, i);
}

template<class KeyType, class DataType>
typename HashTable<KeyType, DataType> :: Iterator HashTable<KeyType, DataType> :: end() const
{
    return Iterator(this, table_size);
}

void gen()
{
    HashTable<int, int> H;
    vector<int> test;
    
    for(int i = 0; i < 1000000; i++)
    {
        //cout << i + 1 << endl;
        int key = rand();
        test.push_back(key);
        H.insert(key, 1);
    }
    
    int count = 0;
    /* for(int i = 0; i < 20; i++)
    {
        H.replace(test[i]);
    } */
    cout << "here1" << endl;
    for(int i = 0; i < 1000000; i++)
    {
        //cout << (H.Find(test[i]) != H.end()) << ' ' << H.Find(test[i]).getIndex() << endl;
        if(H.find(test[i]) == H.end()) { count++; }
    }
    
    cout << count;
    return;
}

int main()
{
    //gen();
    HashTable<string, int> H;
    
    H.insert("pete", 2);
    H.insert("alice", 2);
    
    cout << (H.find("alice") == H.end()) << endl;
    
    H.insert("tom", 5);
    H.replace("pete");
    
    for(HashTable<string, int> :: Iterator it = H.begin(); it != H.end(); ++ it)
    {
        cout << (*it).first << ' ' << (*it).second << endl;
    }
    
    for (const auto& x : H)
    {
        cout << x.first << ' ' << x.second << endl;
    }
    
    return 0;
}
