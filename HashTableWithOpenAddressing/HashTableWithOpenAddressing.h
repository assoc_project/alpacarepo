#ifndef _HASH_TABLE_WITH_OPEN_ADDRESSING_
#define _HASH_TABLE_WITH_OPEN_ADDRESSING_

#define LARGE_PRIME 26227
#define STARTING_CAPACITY 3

#include <functional>
#include <iostream>
#include <vector>
#include <memory>

template<typename KeyType, typename DataType>
class HashTableWithOpenAddressing
{
    struct Slot
    {
        KeyType key;
        DataType data;
        bool isDeleted;
        bool isOccupied;
        Slot()
        {
            isOccupied = false;
            isDeleted = false;
        }
    };
    std::vector<Slot> table;
    std::shared_ptr<std::vector<Slot>> tablePtr;
    size_t tableSize;
    size_t tableCapacity;
    size_t hashFunction(KeyType key)
    {
        return std::hash<KeyType>()(key) * LARGE_PRIME % tableCapacity;
    }
    void capacityIncrease();

public:
    HashTableWithOpenAddressing()
    {
        tableCapacity = STARTING_CAPACITY;
        tableSize = 0;
        table.resize(STARTING_CAPACITY);
        tablePtr = std::make_shared<std::vector<Slot>>(table);
    }
    class HashTableIterator
    {
    public:

        HashTableIterator():iter(nullptr), tablePtr(nullptr){}

        HashTableIterator(std::nullptr_t null): iter(null), tablePtr(null){}

        HashTableIterator(const HashTableIterator& newIter): iter(newIter.iter), tablePtr(newIter.tablePtr){}

        HashTableIterator(const typename std::vector<Slot>::iterator newIter, const typename std::shared_ptr<std::vector<Slot>> newTablePtr): iter(newIter), tablePtr(newTablePtr){}


        typename std::vector<Slot>::iterator operator ->()
        {
            return iter;
        }


        HashTableIterator& operator++()
        {
            ++iter;
            while(iter < tablePtr -> end() && !(iter -> isOccupied))
            {
                ++iter;
            }
            return *this;
        }

        HashTableIterator& operator--()
        {
            --iter;
            while(iter != (tablePtr -> table.begin() - 1) && !(iter -> isOccupied))
            {
                --iter;
            }
            return *this;
        }

        Slot& operator* ()
        {
            return *iter;
        }

        bool operator!= (const HashTableIterator& secIter)
        {
            return (iter != secIter.iter || tablePtr != secIter.tablePtr);
        }

        bool operator== (const HashTableIterator& secIter)
        {
            return iter == secIter.iter && tablePtr == secIter.tablePtr;
        }
    private:
        typename std::shared_ptr<std::vector<Slot>> tablePtr;
        typename std::vector<Slot>::iterator iter;
    };
    typedef HashTableIterator iterator;
    iterator end()
    {
        iterator iter(table.end(), tablePtr);
        return iter;
    }
    iterator begin()
    {
        iterator iter(table.begin(), tablePtr);
        while(iter != end() && !(iter -> isOccupied))
        {
            ++iter;
        }
        return iter;
    }

    iterator insert(const KeyType& key, const DataType& data);

    void erase(const KeyType& delKey);

    iterator find(const KeyType& keyToFind);

    DataType& operator[](const KeyType& keyToFind)
    {
        auto iter = find(keyToFind);
        return (*iter).data;
    }
};


template<typename KeyType, typename DataType>
void HashTableWithOpenAddressing<KeyType, DataType>::capacityIncrease()
{
        std::vector<Slot> dumpTable;
        dumpTable.resize(tableCapacity);
        dumpTable = table;
        table.clear();
        table.resize(tableCapacity * 2);
        tableCapacity *= 2;
        for(size_t i = 0; i < dumpTable.size(); ++i)
        {
            if(dumpTable[i].isOccupied && !dumpTable[i].isDeleted)
            {
                insert(dumpTable[i].key, dumpTable[i].data);
            }
        }
    }

template<typename KeyType, typename DataType>
typename HashTableWithOpenAddressing<KeyType, DataType>::iterator HashTableWithOpenAddressing<KeyType, DataType>::
	find(const KeyType& keyToFind)
{
    iterator iter(table.begin() + hashFunction(keyToFind), tablePtr);
    while( iter != end() && iter -> key != keyToFind)
    {
        ++iter;
    }
    if(iter == end())
    {
        return nullptr;
    }
    else
    {
        return iter;
    }
}

template<typename KeyType, typename DataType>
void HashTableWithOpenAddressing<KeyType, DataType>::
	erase(const KeyType& delKey)
{
    size_t curPosition = hashFunction(delKey);
    while((table[curPosition].isOccupied || table[curPosition].isDeleted) && table[curPosition].key != delKey && curPosition < tableCapacity)
    {
        ++curPosition;
    }
    if(table[curPosition].key == delKey)
    {
        table[curPosition].isDeleted = true;
    }
}

template<typename KeyType, typename DataType>
typename HashTableWithOpenAddressing<KeyType, DataType>::iterator HashTableWithOpenAddressing<KeyType, DataType>::
	insert(const KeyType& key, const DataType& data)
{
    size_t curPosition = hashFunction(key);
    while(table[curPosition].isOccupied && curPosition < tableSize)
    {
        ++curPosition;
    }
    if(curPosition == tableCapacity)
    {
        capacityIncrease();
        return insert(key, data);
    }
    else
    {
        iterator iter(table.begin() + curPosition, tablePtr);
        (*iter).key = key;
        (*iter). data = data;
        (*iter).isOccupied = true;
        tableSize++;
        return iter;
    }
}

#endif // _HASH_TABLE_WITH_OPEN_ADDRESSING_
