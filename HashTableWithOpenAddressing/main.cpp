#include "HashTableWithOpenAddressing.h"

int main()
{
    HashTableWithOpenAddressing<int, int> hashT;
    hashT.insert(1, 2);
    hashT.insert(2, 3);
    hashT.insert(3, 2);
    hashT.insert(4, 2);
    hashT.insert(5, 2);
    hashT.insert(6, 2);
    hashT.insert(123, 228);
    hashT.erase(2);
    hashT.erase(6);
    std::cout << "\n" << (hashT.find(123) -> data);
    return 0;
}
